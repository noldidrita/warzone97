package view;

import javafx.animation.Animation;
import javafx.animation.PathTransition;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class CircularButton extends StackPane {
	public Circle circle;
	public String text;
	public PathTransition transition1, transition2;
	
	public CircularButton(double radius, Color circleColor, Color hoverColor,Color c1Color, Color c2Color, double c1Radius, double c2Radius, String text){
		super();
		circle = new Circle(radius);
		circle.setFill(circleColor);
		Circle c1 = new Circle(c1Radius);
		Circle c2 = new Circle(c2Radius);
		c1.setFill(c1Color);
		c2.setFill(c2Color);

		transition1 = new PathTransition(Duration.seconds(3), circle, c1);
		transition1.setCycleCount(Animation.INDEFINITE);
		transition1.play();
		Text label = new Text(text);
		label.setFont(new Font("Hobo Std", 20));
		label.setFill(Color.WHITESMOKE);
		transition2 = new PathTransition(Duration.seconds(3), circle, c2);
		transition2.setCycleCount(Animation.INDEFINITE);
		transition2.setRate(-1);
		transition2.play();
		setOnMouseEntered(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				c1.setRadius(2*c1Radius);
				c2.setRadius(2*c2Radius);
				transition1.setRate(3);
				transition2.setRate(-3);
				circle.setFill(hoverColor);
			}
		});
		
		setOnMouseExited(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				transition1.setRate(1);
				transition2.setRate(-1);
				c1.setRadius(c1Radius);
				c2.setRadius(c2Radius);
				circle.setFill(circleColor);
			}
		});
		getChildren().addAll(circle, c1, c2, label);
	}
}
