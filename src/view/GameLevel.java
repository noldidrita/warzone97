package view;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import model.Brick;
import model.Bullet;
import model.Treasure;
import model.GameObject;
import model.GameObject.Direction;
import model.Tank;

public class GameLevel {

	private static final String DEFAULT_BACKGROUND = "grass.png";
	private static final int DEFAULT_NR_ENEMIES = 10;
	private static final int DEFAULT_ENEMY_LIMIT = 10;

	GridPane levelGrid;
	BorderPane screen;
	IntegerProperty enemies;
	Pane mainPane;
	Pane levelPane;
	BorderPane HUD;
	ArrayList<GameObject> gameObjects;
	Background background;
	Tank[] player;
	double[][] targetPosition;
	Scene scene;
	private int nrPlayers;
	Timeline spawner;
	int nrEnemies;
	int enemyLimit;
	Random random;
	String levelName;
	double[] eaglePosition;
	Treasure theEagle;

	public Pane getLevelPane() {
		return levelPane;
	}

	public ArrayList<GameObject> getGameObjects() {
		return gameObjects;
	}

	public void pauseGame(boolean pause) {
		if (pause)
			spawner.stop();
		else
			spawner.play();
		for (int i = 0; i < gameObjects.size(); i++) {
			GameObject curr = gameObjects.get(i);
			if (curr instanceof Bullet) {
				if (pause)
					((Bullet) curr).pauseAnimations();
				else
					((Bullet) curr).resumeAnimations();
			} else if (curr instanceof Tank) {
				if (pause)
					((Tank) curr).pauseAnimations();
				else
					((Tank) curr).resumeAnimations();
			}
		}
	}

	public void setTargetPosition(int targetNr, double x, double y) {
		targetPosition[targetNr][0] = x;
		targetPosition[targetNr][1] = y;
	}

	public double[] getTargetPosition(int playerNr) {
		return targetPosition[playerNr];
	}

	public GameLevel(String levelName, int nrPlayers) {
		gameObjects = new ArrayList<>();
		this.nrPlayers = nrPlayers;
		player = new Tank[nrPlayers];
		random = new Random();
		player[0] = new Tank(GameObject.OBJECT_TYPE_PLAYER, SceneManager
				.getResources().getImage("tank0.png"), 100, 3, 0, 800, 10, 5,
				SceneManager.getResources().getImage("bullet.png"), 3, -1);
		gameObjects.add(player[0]);
		if (nrPlayers == 2) {
			player[1] = new Tank(GameObject.OBJECT_TYPE_PLAYER, SceneManager
					.getResources().getImage("tank1.png"), 100, 3, 1, 1000, 10,
					5, SceneManager.getResources().getImage("bullet.png"), 3,
					-1);
			gameObjects.add(player[1]);
		}
		targetPosition = new double[nrPlayers + 1][2];
		nrEnemies = 0;
		this.levelName = levelName;
		screen = new BorderPane();
		levelPane = new Pane();
		levelGrid = loadLevel();
		HUD = loadHUD();
		levelPane.getChildren().add(levelGrid);
		screen.setCenter(levelPane);
		screen.setBottom(HUD);

		scene = new Scene(screen);
		KeyboardHandler handler = new KeyboardHandler();
		scene.setOnKeyPressed(handler);
		scene.setOnKeyReleased(handler);
		spawner = new Timeline(new KeyFrame(Duration.millis(3000),
				new EventHandler<ActionEvent>() {
					int lastTarget = 0;
					
					@Override
					public void handle(ActionEvent event) {
						
						boolean p1Dead = player[0].getLives() < 0
								&& player[0].isDead() == true;
						boolean p2Dead = nrPlayers==2 && player[1] != null
								&& player[1].getLives() < 0
								&& player[1].isDead() == true;

						if (p1Dead && p2Dead) {
							pauseGame(true);
							SceneManager.onGameOver(false, nrPlayers);
						}
						if(targetPosition[nrPlayers][0]==0 && targetPosition[nrPlayers][1]==0){
							Bounds eagleB = theEagle.getBoundsInParent();
							setTargetPosition(nrPlayers, eagleB.getMinX()+eagleB.getWidth()/2, eagleB.getMinY()+eagleB.getHeight()/2);
						}
						if (player[0] != null && player[0].isDead()
								&& player[0].getLives() >= 0) {
							double[] sp = findSpawnPoint(player[0]);
							player[0].draw(levelPane, sp[0], sp[1]);
							setTargetPosition(0, sp[0], sp[1]);
						} else if (nrPlayers == 2 && player[1] != null
								&& player[1].isDead()
								&& player[1].getLives() >= 0) {
							double[] sp = findSpawnPoint(player[1]);
							player[1].draw(levelPane, sp[0], sp[1]);
							setTargetPosition(1, sp[0], sp[1]);
						} else if (nrEnemies < enemyLimit && enemies.get() > 0) {
							int eNr = random.nextInt(6) + 2;
							Tank enemy;
							switch (eNr) {
							case 2:
								enemy = new Tank(GameObject.OBJECT_TYPE_ENEMY,
										SceneManager.getResources().getImage(
												"tank" + eNr + ".png"), 10, 1,
										eNr, 1000, 5, 5, SceneManager
												.getResources().getImage(
														"bullet.png"), 3,
														nrPlayers);
								break;
							case 3:
								enemy = new Tank(GameObject.OBJECT_TYPE_ENEMY,
										SceneManager.getResources().getImage(
												"tank" + eNr + ".png"), 15, 1,
										eNr, 1000, 5, 5, SceneManager
												.getResources().getImage(
														"bullet.png"), 2,
														lastTarget);
								break;
							case 4:
								enemy = new Tank(GameObject.OBJECT_TYPE_ENEMY,
										SceneManager.getResources().getImage(
												"tank" + eNr + ".png"), 15, 1,
										eNr, 1000, 10, 5, SceneManager
												.getResources().getImage(
														"bullet.png"), 2,
														lastTarget);
								break;
							case 5:
								enemy = new Tank(GameObject.OBJECT_TYPE_ENEMY,
										SceneManager.getResources().getImage(
												"tank" + eNr + ".png"), 20, 1,
										eNr, 1000, 15, 5, SceneManager
												.getResources().getImage(
														"bullet.png"), 1,
														lastTarget);
								break;
							case 6:
								enemy = new Tank(GameObject.OBJECT_TYPE_ENEMY,
										SceneManager.getResources().getImage(
												"tank" + eNr + ".png"), 20, 1,
										eNr, 1000, 17, 5, SceneManager
												.getResources().getImage(
														"bullet.png"), 1,
														lastTarget);
								break;
							case 7:
								enemy = new Tank(GameObject.OBJECT_TYPE_ENEMY,
										SceneManager.getResources().getImage(
												"tank" + eNr + ".png"), 25, 1,
										eNr, 1000, 15, 5, SceneManager
												.getResources().getImage(
														"bullet.png"), 1,
														lastTarget);
								break;
								
							default:
								enemy = new Tank(GameObject.OBJECT_TYPE_ENEMY,
										SceneManager.getResources().getImage(
												"tank" + eNr + ".png"), 30, 1,
										eNr, 1000, 10, 5, SceneManager
												.getResources().getImage(
														"bullet.png"), 2,
														lastTarget);
							}
							if(nrPlayers==2)
								lastTarget = 1 - lastTarget;

							double[] sp = findSpawnPoint(enemy);
							enemy.draw(levelPane, sp[0], sp[1]);
							gameObjects.add(enemy);
							enemies.set(enemies.get() - 1);
							nrEnemies++;
						} else if (enemies.get() == 0 && nrEnemies == 0) {
							spawner.stop();
							SceneManager.onGameOver(true, nrPlayers);
						}
					}
				}));
		spawner.setCycleCount(Animation.INDEFINITE);
		spawner.play();

	}

	public Random getRandom() {
		return random;
	}

	public Scene getScene() {
		return scene;
	}

	public GridPane loadLevel() {
		File levelFile = new File("levels/" + levelName + ".txt");
		GridPane res = new GridPane();
		try {
			Scanner sc = new Scanner(levelFile);
			SceneManager.GRID_SIZE_Y = sc.nextInt();
			SceneManager.GRID_SIZE_X = sc.nextInt();

			for (int i = 0; i < SceneManager.GRID_SIZE_Y; i++) {
				for (int j = 0; j < SceneManager.GRID_SIZE_X; j++) {
					int type = sc.nextInt();
					if (type == 0)
						continue;
					try {

						GameObject current;
						if (type == 9) {
							Image img = SceneManager.getResources().getImage("eagle.png");
							theEagle = new Treasure(img);
							current = theEagle;
						} else{
							Image img = SceneManager.getResources().getImage("wall"+
									type + ".png");
							current = new Brick(type, img);
						}
						gameObjects.add(current);
						res.add(current, j, i);
					} catch (Exception e) {
						System.out.println("Image " + type + ".png not found");
					}
				}
			}
			if (sc.hasNextInt())
				enemies = new SimpleIntegerProperty(sc.nextInt());
			else
				enemies = new SimpleIntegerProperty(DEFAULT_NR_ENEMIES);
			String bg;
			if (sc.hasNextInt())
				enemyLimit = sc.nextInt();
			else
				enemyLimit = DEFAULT_NR_ENEMIES;
			if (sc.hasNext())
				bg = sc.next();
			else
				bg = DEFAULT_BACKGROUND;
			Image bgImg = SceneManager.getResources().getImage(bg);
			background = new Background(new BackgroundImage(bgImg, null, null,
					null, null));
			levelPane.setBackground(background);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return res;
	}

	public BorderPane loadHUD() {
		BorderPane res = new BorderPane();
		res.setPadding(new Insets(15, 20, 18, 20));
		
		VBox player1 = new VBox();
		
		Label p1Name = new Label(SceneManager.getName(1));
		p1Name.setFont(new Font("Tahoma", 20));
		p1Name.setTextFill(Color.WHITE);
		
		Label p1Lives = new Label();
		p1Lives.setFont(new Font("Tahoma", 15));
		p1Lives.setTextFill(Color.WHITE);
		p1Lives.textProperty().bind(
				Bindings.concat("Lives: ",
						Bindings.convert(player[0].getLivesProperty())));
		VBox.setMargin(p1Lives, new Insets(12, 0, 12, 0));
		
		ProgressBar p1Health = new ProgressBar();
		p1Health.progressProperty().bind(
				Bindings.divide(player[0].getHealthPoints(), 100.0));
		p1Health.setMinWidth(125);
		player1.getChildren().addAll(p1Name, p1Lives, p1Health);
		
		res.setLeft(player1);

		if (nrPlayers == 2) {
			VBox player2 = new VBox();
			player2.setAlignment(Pos.CENTER_RIGHT);
			Label p2Name = new Label(SceneManager.getName(2));
			p2Name.setTextFill(Color.WHITE);

			p2Name.setFont(new Font("Tahoma", 20));
			Label p2Lives = new Label();
			p2Lives.setFont(new Font("Tahoma", 15));
			p2Lives.setTextFill(Color.WHITE);

			p2Lives.textProperty().bind(
					Bindings.concat("Lives: ",
							Bindings.convert(player[1].getLivesProperty())));
			VBox.setMargin(p2Lives, new Insets(12, 0, 12, 0));
			ProgressBar p2Health = new ProgressBar();
			p2Health.progressProperty().bind(
					Bindings.divide(player[1].getHealthPoints(), 100.0));
			p2Health.setMinWidth(125);
			player2.getChildren().addAll(p2Name, p2Lives, p2Health);
			res.setRight(player2);
		}

		VBox levelInfo = new VBox();
		Label levelLabel = new Label(levelName);
		levelLabel.setFont(new Font("Tahoma", 20));
		levelLabel.setTextFill(Color.WHITE);

		Label lblEnemies = new Label();
		lblEnemies.setFont(new Font("Tahoma", 15));
		lblEnemies.setTextFill(Color.WHITE);

		lblEnemies.textProperty().bind(
				Bindings.concat(Bindings.convert(enemies), " enemies left"));
		Label scoreLabel = new Label();
		scoreLabel.setFont(new Font("Tahoma", 15));
		scoreLabel.setTextFill(Color.WHITE);

		scoreLabel.textProperty().bind(
				Bindings.concat("Score: ",
						Bindings.convert(SceneManager.getScoreProperty())));
		VBox.setMargin(lblEnemies, new Insets(12, 0, 12, 0));
		levelInfo.getChildren().addAll(levelLabel, lblEnemies, scoreLabel);
		levelInfo.setAlignment(Pos.CENTER);
		res.setCenter(levelInfo);
		Background bg = new Background(new BackgroundImage(SceneManager
				.getResources().getImage("hudbg.png"), null, null, null, null));
		res.setBackground(bg);
		return res;
	}

	public void draw(Node n) {
		levelPane.getChildren().add(n);
	}

	public void removeObjectFromList(GameObject o) {
		FadeTransition fade = new FadeTransition(Duration.millis(500), o);
		fade.setFromValue(1);
		fade.setToValue(0);
		fade.setCycleCount(1);
		fade.play();

		if (o.getType() == GameObject.OBJECT_TYPE_ENEMY)
			nrEnemies--;
		gameObjects.remove(o);
	}

	public void addObjectToList(GameObject o) {
		gameObjects.add(o);
	}

	public int getNrPlayers() {
		return nrPlayers;
	}

	public void setNrPlayers(int nrPlayers) {
		this.nrPlayers = nrPlayers;
	}
	
	public void removeNodeFromPane(Node n) {
		levelPane.getChildren().remove(n);
	}

	public double getWidth() {
		return levelPane.getWidth();
	}

	public double getHeight() {
		return levelPane.getHeight();
	}

	public void onMove(GameObject object) {
		for (int i = 0; i < gameObjects.size(); i++) {
			GameObject curr = gameObjects.get(i);
			if (curr == object || !curr.isVisible())
				continue;
			if(object.getType()==GameObject.OBJECT_TYPE_BULLET && object.getType()==GameObject.OBJECT_TYPE_POWERUP)
				continue;
			if (curr.getBoundsInParent().intersects(object.getBoundsInParent())) {
				curr.onCollide(object);
				object.onCollide(curr);
			}
		}
	}

	public boolean canMove(GameObject object, double x, double y) {
		Bounds bounds = object.getBoundsInParent();
		if (x < 0 || x > levelPane.getWidth() - bounds.getWidth() || y < 0
				|| y > levelPane.getHeight() - bounds.getHeight())
			return false;

		for (int i = 0; i < gameObjects.size(); i++) {
			GameObject curr = gameObjects.get(i);
			if (curr == object || !curr.isVisible())
				continue;
			if (curr.getType() == GameObject.OBJECT_TYPE_POWERUP)
				continue;
			if (curr.getBoundsInParent().intersects(x, y, bounds.getWidth(),
					bounds.getHeight())) {
				return false;
			}
		}
		return true;
	}

	public double[] findSpawnPoint(GameObject o) {
		double[] res = new double[2];
		Random r = new Random(System.currentTimeMillis());
		do {
			res[0] = r.nextInt((int) levelPane.getWidth());
			res[1] = r.nextInt((int) levelPane.getHeight());
		} while (!canMove(o, res[0], res[1]));
		return res;
	}

	private class KeyboardHandler implements EventHandler<KeyEvent> {

		@Override
		public void handle(KeyEvent event) {
			if (event.getEventType() == KeyEvent.KEY_PRESSED) {
				if (!SceneManager.getPaused()) {
					if (event.getCode() == KeyCode.ESCAPE)
						SceneManager.pauseGame();
					if (player[0] != null && !player[0].isDead()) {
						switch (event.getCode()) {
						case UP:
							player[0].onMove(Direction.UP);
							return;
						case DOWN:
							player[0].onMove(Direction.DOWN);
							return;
						case LEFT:
							player[0].onMove(Direction.LEFT);
							return;
						case RIGHT:
							player[0].onMove(Direction.RIGHT);
							return;
						case NUMPAD0:
							player[0].onShoot();
							return;
						}
					}
					if (nrPlayers == 2 && player[1] != null
							&& !player[1].isDead()) {
						switch (event.getCode()) {
						case W:
							player[1].onMove(Direction.UP);
							return;
						case S:
							player[1].onMove(Direction.DOWN);
							return;
						case A:
							player[1].onMove(Direction.LEFT);
							return;
						case D:
							player[1].onMove(Direction.RIGHT);
							return;
						case SPACE:
							player[1].onShoot();
							return;
						}
					}
				}
			} else if (event.getEventType() == KeyEvent.KEY_RELEASED) {
				KeyCode code = event.getCode();
				if (!SceneManager.getPaused()) {
					if (player[0] != null
							&& !player[0].isDead()
							&& ((code == KeyCode.UP && player[0].getDirection() == Direction.UP)
									|| (code == KeyCode.DOWN && player[0]
											.getDirection() == Direction.DOWN)
									|| (code == KeyCode.LEFT && player[0]
											.getDirection() == Direction.LEFT) || (code == KeyCode.RIGHT && player[0]
									.getDirection() == Direction.RIGHT)))
						player[0].onStop();
					else if (nrPlayers == 2
							&& player[1] != null
							&& !player[1].isDead()
							&& ((code == KeyCode.W && player[1].getDirection() == Direction.UP)
									|| (code == KeyCode.S && player[1]
											.getDirection() == Direction.DOWN)
									|| (code == KeyCode.A && player[1]
											.getDirection() == Direction.LEFT) || (code == KeyCode.D && player[1]
									.getDirection() == Direction.RIGHT)))
						player[1].onStop();
				}
			}
		}
	}

	public double[] getPosition(int object) {
		return targetPosition[object];
	}
}
