package view;
	
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Scene;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import model.ResourceManager;
import model.SoundManager;


public class SceneManager extends Application {
	
	public static int BLOCK_SIZE = 48;
	public static int GRID_SIZE_X;
	public static int GRID_SIZE_Y;

	private static IntegerProperty Score;
	private static String[] Name;
	private static Stage primaryStage;
	private static GameLevel currentLevel;
	private static ResourceManager resManager;
	private static Stage pauseWindow;
	private static Boolean paused;
	private static SoundManager player;
	
	@Override
	public void start(Stage primaryStage) {
		SceneManager.primaryStage = primaryStage;
		resManager = new ResourceManager();
		pauseWindow = new Stage();
		Score = new SimpleIntegerProperty(0);
		Name = new String[2];
		player = new SoundManager();
		player.GamePlaySound();
		primaryStage.setScene(MenuManager.getStartMenu());
		primaryStage.setTitle("War Zone 97");
		primaryStage.getIcons().add(resManager.getImage("icon.png"));
		primaryStage.setResizable(false);
		primaryStage.show();
	}
	
	public static void setScene(Scene newScene){
		primaryStage.setScene(newScene);
		primaryStage.show();
	}
	
	public static SoundManager getSoundManager(){
		return player;
	}
	
	public static ResourceManager getResources(){
		return resManager;
	}
	
	public static int getScore() {
		return Score.get();
	}

	public static void setScore(int score) {
		Score.set(score);
	}

	public static String getName(int playerNr) {
		if(Name[playerNr-1]!=null && !Name[playerNr-1].isEmpty())
			return Name[playerNr-1];
		return "Player "+playerNr;
	}

	public static void setName(int index, String name) {
		Name[index-1] = name;
	}

	public static GameLevel getLevel() {
		return currentLevel;
	}

	public static void updateScore(int sc){
		Score.set(Score.get()+sc);
	}
	
	public static void startLevel(String lvl, int nrPlayers) {
		currentLevel = new GameLevel(lvl, nrPlayers);
		paused = false;
		primaryStage.setScene(currentLevel.getScene());
	}

	public static void onGameOver(boolean wonLevel, int nrPlayers) {
		currentLevel.getGameObjects().clear();
		currentLevel.getLevelPane().getChildren().clear();
		currentLevel = null;
		if(!wonLevel)
			primaryStage.setScene(MenuManager.getScoreboard(true, nrPlayers));
		else primaryStage.setScene(MenuManager.getNextLevelMenu(nrPlayers));
	}
	
	public static void pauseGame(){
		paused=true;
		currentLevel.pauseGame(true);
		pauseWindow.setScene(MenuManager.getPauseMenu());
		pauseWindow.show();
	}
	
	public static void closeGame(){
		pauseWindow.close();
		currentLevel.getGameObjects().clear();
		currentLevel.getLevelPane().getChildren().clear();
		primaryStage.setScene(MenuManager.getStartMenu());
		currentLevel = null;
	}
	public static void resumeGame(){
		pauseWindow.close();
		paused=false;
		currentLevel.pauseGame(false);
	}
	
	public static void main(String[] args) {
		launch(args);
	}

	public static IntegerProperty getScoreProperty() {
		return Score;
	}

	public static Boolean getPaused() {
		return paused;
	}

	public static void setPaused(Boolean paused) {
		SceneManager.paused = paused;
	}

}
