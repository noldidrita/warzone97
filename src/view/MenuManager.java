package view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.ScoreEntry;

public class MenuManager {

	public static Scene getPlayMenu(int nrPlayers) {
		VBox vbox = new VBox();

		Label gameTitle = new Label();
		if (nrPlayers == 1)
			gameTitle.setText("SinglePlayer");
		else
			gameTitle.setText("Multiplayer");

		gameTitle.setFont(Font.font("Maiandra GD", 25));
		HBox playerArea = new HBox();
		VBox p1 = new VBox();
		Label p1lbl = new Label("Player 1 Name:");
		TextField p1Name = new TextField();
		p1.getChildren().addAll(p1lbl, p1Name);
		playerArea.getChildren().add(p1);
		HBox.setMargin(p1, new Insets(10));

		TextField p2Name = new TextField();
		if (nrPlayers == 2) {
			VBox p2 = new VBox();
			Label p2lbl = new Label("Player 2 Name:");
			p2.getChildren().addAll(p2lbl, p2Name);
			HBox.setMargin(p2, new Insets(10));
			playerArea.getChildren().add(p2);
		}
		playerArea.setAlignment(Pos.CENTER);
		vbox.getChildren().add(playerArea);

		Label level = new Label("Level");
		ComboBox<String> levelCombo = new ComboBox<>();
		Text error = new Text("You must select a level");
		error.setFill(Color.RED);
		error.setVisible(false);
		levelCombo.setMinWidth(150);
		VBox.setMargin(levelCombo, new Insets(0, 0, 15, 0));
		Scanner sc;
		try {
			sc = new Scanner(new File("levels.cfg"));
			while (sc.hasNext()) {
				String levelName = sc.next();
				levelCombo.getItems().add(levelName);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		vbox.setAlignment(Pos.CENTER);
		vbox.setPadding(new Insets(15));

		Color circleColor = Color.DARKKHAKI;
		Color hoverColor = Color.MAROON;
		Color c1Color = Color.TEAL;
		Color c2Color = Color.ROYALBLUE;
		double circleRadius = 60, c1R = 5, c2R = 5;
		CircularButton start = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "Start Game");
		CircularButton back = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "Go Back");
		HBox hbox = new HBox();
		hbox.getChildren().addAll(start, back);
		HBox.setMargin(start, new Insets(10));
		HBox.setMargin(back, new Insets(10, 10, 10, 60));

		start.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event arg0) {
				String level = levelCombo.getSelectionModel().getSelectedItem();
				if (level == null)
					error.setVisible(true);
				else {
					if (!p1Name.getText().equals(""))
						SceneManager.setName(1, p1Name.getText());
					if (nrPlayers == 2 && p2Name != null
							&& !p2Name.getText().equals(""))
						SceneManager.setName(2, p2Name.getText());
					SceneManager.setScore(0);
					SceneManager.startLevel(level, nrPlayers);
				}
			}
		});

		back.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				SceneManager.setScene(MenuManager.getStartMenu());
			}
		});
		vbox.getChildren().addAll(level, levelCombo, error, hbox);
		Scene scene = new Scene(vbox);
		return scene;
	}

	public static Scene getScoreboard(boolean addEntry, int nrPlayers) {
		FileInputStream fis;
		ArrayList<ScoreEntry> scoreList = new ArrayList<>();
		try {
			fis = new FileInputStream("scores.sr");
			ObjectInputStream ois = new ObjectInputStream(fis);
			scoreList = (ArrayList<ScoreEntry>) ois.readObject();
		} catch (FileNotFoundException e) {

		} catch (ClassNotFoundException e) {

		} catch (IOException e) {

		}
		if (addEntry) {
			Date today = new Date();
			SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy hh:mm");
			String name = SceneManager.getName(1);
			if (nrPlayers == 2)
				name += " and " + SceneManager.getName(2);
			scoreList.add(new ScoreEntry(name, format.format(today),
					SceneManager.getScore()));
			scoreList.sort(null);

			try {
				FileOutputStream fos = new FileOutputStream("scores.sr");
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(scoreList);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		ListView lv = new ListView();

		if (scoreList.size() == 0)
			lv.getItems()
					.add("The scores list is empty. Play the game to see your name appear in the list");
		else
			lv.getItems().addAll(scoreList.toArray());
		lv.setMaxWidth(500);
		lv.setMaxHeight(500);

		VBox box = new VBox();
		Color circleColor = Color.ORANGE;
		Color hoverColor = Color.ORANGERED;
		Color c1Color = Color.SEAGREEN;
		Color c2Color = Color.TEAL;
		double circleRadius = 50, c1R = 5, c2R = 5;

		CircularButton back = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "Go Back");
		back.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				SceneManager.setScene(MenuManager.getStartMenu());
			}
		});
		box.getChildren().addAll(lv, back);
		Scene scene = new Scene(box, 500, 500);
		
		return scene;
	}

	public static Scene getPauseMenu() {
		VBox mainBox = new VBox();
		mainBox.setAlignment(Pos.CENTER);
		Text paused = new Text("Paused");
		paused.setFont(new Font("Tahoma", 25));
		paused.setFill(Color.CORNFLOWERBLUE);
		mainBox.getChildren().add(paused);
		HBox options = new HBox();
		VBox.setMargin(paused, new Insets(10, 0, 25, 0));
		Color circleColor = Color.LIME;
		Color hoverColor = Color.DARKTURQUOISE;
		Color c1Color = Color.PALEGOLDENROD;
		Color c2Color = Color.SLATEGREY;
		double circleRadius = 80, c1R = 7, c2R = 7;
		CircularButton mainMenu = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "Go to Main Menu");
		mainMenu.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				SceneManager.closeGame();
			}
		});
		CircularButton back = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "Continue");
		back.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				SceneManager.resumeGame();
			}
		});
		CircularButton quit = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "Quit");
		quit.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				Platform.exit();
			}
		});
		HBox.setMargin(mainMenu, new Insets(0, 15, 0, 30));
		HBox.setMargin(back, new Insets(0, 15, 0, 15));
		HBox.setMargin(quit, new Insets(0, 30, 0, 15));

		options.getChildren().addAll(mainMenu, back, quit);
		mainBox.getChildren().add(options);
		mainBox.setPadding(new Insets(15));
		Scene scene = new Scene(mainBox);
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if(event.getCode()==KeyCode.ESCAPE)
					SceneManager.resumeGame();
			}
		});
		return scene;
	}

	public static Scene getStartMenu() {
		HBox box = new HBox();
		Color circleColor = Color.LIGHTBLUE;
		Color hoverColor = Color.DARKBLUE;
		Color c1Color = Color.PINK;
		Color c2Color = Color.CORAL;
		double circleRadius = 75, c1R = 7, c2R = 7;
		CircularButton singlePlayer = new CircularButton(circleRadius,
				circleColor, hoverColor, c1Color, c2Color, c1R, c2R,
				"Single Player");
		singlePlayer.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SceneManager.setScene(getPlayMenu(1));
			}
		});
		CircularButton multiPlayer = new CircularButton(circleRadius,
				circleColor, hoverColor, c1Color, c2Color, c1R, c2R,
				"Multiplayer");
		multiPlayer.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SceneManager.setScene(getPlayMenu(2));
			}
		});

		CircularButton scoreBoard = new CircularButton(circleRadius,
				circleColor, hoverColor, c1Color, c2Color, c1R, c2R,
				"Scoreboard");
		scoreBoard.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SceneManager.setScene(getScoreboard(false, -1));
			}
		});

		CircularButton about = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "About");
		about.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SceneManager.setScene(getAboutMenu());
			}
		});
		CircularButton quit = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "Quit");
		quit.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				Platform.exit();
			}
		});
		box.setPadding(new Insets(100));
		box.getChildren().addAll(singlePlayer, multiPlayer, scoreBoard, about,
				quit);
		HBox.setMargin(singlePlayer, new Insets(10));
		HBox.setMargin(multiPlayer, new Insets(10));
		HBox.setMargin(scoreBoard, new Insets(10));
		HBox.setMargin(about, new Insets(10));
		HBox.setMargin(quit, new Insets(10));
		box.setBackground(new Background(new BackgroundImage(SceneManager.getResources().getImage("menuBackground.jpg"), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, null, new BackgroundSize(1200, 400, false, false, false, false))));
		Scene scene = new Scene(box);
		return scene;
	}
	
	public static Scene getAboutMenu(){
		
		VBox mainBox = new VBox();
		Text i1 = new Text("This game is developed by Arnold Drita for his Java project assignment.");
		Text i2 = new Text("It's main scope is to show the utilization of JavaFX api and Object Oriented Programming practices.");
		Text i3 = new Text("Although many of the features could be implemented differently for better performance, they are implemented this way just for simplicity.");
		Text i4 = new Text("The controls are as follows: .");
		Text i5 = new Text("Player 1: W->Up, D->Right, S->Down, A->Left, Space->Shoot");
		Text i6 = new Text("Player 2: UP Arrow->Up, Right Arrow->Right, Down Arrow->Down, Left Arrow->Left, NumPad 0->Shoot");
		Text i7 = new Text("New levels can easily be added by following the instructions located at the root directory of the game, in the file called ReadMe.txt");
		
		Color circleColor = Color.ROYALBLUE;
		Color hoverColor = Color.MEDIUMSLATEBLUE;
		Color c1Color = Color.TURQUOISE;
		Color c2Color = Color.TOMATO;
		double circleRadius = 75, c1R = 7, c2R = 7;
		CircularButton back = new CircularButton(circleRadius,
				circleColor, hoverColor, c1Color, c2Color, c1R, c2R,
				"Go Back");
		back.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				SceneManager.setScene(getStartMenu());
			}
		});
		VBox.setMargin(back, new Insets(20));
		mainBox.setPadding(new Insets(20, 15, 10, 15));
		mainBox.getChildren().addAll(i1, i2, i3, i4, i5, i6, i7, back);
		mainBox.setAlignment(Pos.CENTER);
		Scene scene = new Scene(mainBox);
		return scene;
	}

	public static Scene getNextLevelMenu(int nrPlayers) {
		VBox vbox = new VBox();

		Label gameTitle = new Label();

		gameTitle.setText("Congratulations! You won this level. Now select the next one.");
		gameTitle.setFont(Font.font("Maiandra GD", 25));
		
		Label level = new Label("Level");
		ComboBox<String> levelCombo = new ComboBox<>();
		Text error = new Text("You must select a level");
		error.setFill(Color.RED);
		error.setVisible(false);
		levelCombo.setMinWidth(150);
		VBox.setMargin(levelCombo, new Insets(0, 0, 15, 0));
		Scanner sc;
		try {
			sc = new Scanner(new File("levels.cfg"));
			while (sc.hasNext()) {
				String levelName = sc.next();
				levelCombo.getItems().add(levelName);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		vbox.setAlignment(Pos.CENTER);
		vbox.setPadding(new Insets(15));

		Color circleColor = Color.LIGHTBLUE;
		Color hoverColor = Color.DARKBLUE;
		Color c1Color = Color.PINK;
		Color c2Color = Color.CORAL;
		double circleRadius = 60, c1R = 5, c2R = 5;
		CircularButton start = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "Start Game");
		CircularButton back = new CircularButton(circleRadius, circleColor,
				hoverColor, c1Color, c2Color, c1R, c2R, "Go Back");
		HBox hbox = new HBox();
		hbox.getChildren().addAll(start, back);
		HBox.setMargin(start, new Insets(10));
		HBox.setMargin(back, new Insets(10, 10, 10, 60));

		start.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event arg0) {
				String level = levelCombo.getSelectionModel().getSelectedItem();
				if (level == null)
					error.setVisible(true);
				else {
					SceneManager.startLevel(level, nrPlayers);
				}
			}
		});

		back.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				SceneManager.setScene(MenuManager.getScoreboard(true, nrPlayers));
			}
		});
		vbox.getChildren().addAll(gameTitle, level, levelCombo, error, hbox);
		Scene scene = new Scene(vbox);
		return scene;
	}
	
}
