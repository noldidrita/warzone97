package model;

import javafx.scene.image.Image;
import javafx.scene.media.MediaPlayerBuilder;
import view.SceneManager;

public class Brick extends GameObject{
	
	private int brickType;
	private int hp;
	
	public Brick(int type, Image img) {
		super(OBJECT_TYPE_BRICK, img);
		brickType = type;
		hp = getHP(type);
	}
		
	private static int getHP(int type){
		int hp = 0;
		switch(type){
		case 1:
			hp = 20;
			break;
		case 2:
			hp = 18;
			break;
		case 3:
			hp = 25;
			break;
		case 4:
			hp = 25;
			break;
		case 5:
			hp = 9999999;;
			break;
		case 6:
			hp = 10;
			break;
		case 7:
			hp = 7;
			break;
		case 8:
			hp = 12;
			break;
		}
		return hp;
	}

	@Override
	public boolean onCollide(GameObject object) {
		if(object.getType()==OBJECT_TYPE_BULLET){
			if(brickType!=5){
				hp-=((Bullet)object).getDamage();
				SceneManager.getSoundManager().BrickHit();
			} else {
				SceneManager.getSoundManager().BulletOnMetal();
			}
				if(hp<=0){
					if(((Bullet)object).getSource()==OBJECT_TYPE_PLAYER)
						SceneManager.updateScore(5*brickType);
					SceneManager.getLevel().removeObjectFromList(this);
				}
			}
		return true;
	}
}
