package model;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.image.Image;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;

public class ResourceManager {
	
    private Map<String, Image> sprites;
    private Map<String, AudioClip> sounds;

    //private <String, Clip> sounds = new HashMap<>();

    public ResourceManager(){
    	sprites = new HashMap<>();
    	sounds = new HashMap<>();
    }
    public Image getImage(String name){
    	Image img;
    	if(sprites.containsKey(name)){
    		img = sprites.get(name);
    	}else{
	    	img = new Image("images/"+name);
	    	sprites.put(name, img);
    	}
    	return img;
    }
    
    public AudioClip getSound(String name){
    	AudioClip media;
    	if(sounds.containsKey(name)){
    		media = sounds.get(name);
    	}else{
	    	media = new AudioClip(new File("sounds/"+name).toURI().toString());
	    	sounds.put(name, media);
    	}
    	return media;
    }
    
   /* public Image getImage(String name, int width, int height){
    	Image img;
    	if(sprites.containsKey(name)){
    		img = sprites.get(name);
    		if((img.getRequestedHeight()==height||height==-1) && (img.getRequestedWidth() == width || width==-1))
    			return img;
    	}
    	if(width==-1 && height==-1)
    		img = new Image(name);
    	else img = new Image(name, width, height, false, true);
    	sprites.put(name, img);
    	return img;
    }*/
}