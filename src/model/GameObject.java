package model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class GameObject extends ImageView {
	//Object type constants
	public static final int OBJECT_TYPE_BRICK = 0;
	public static final int OBJECT_TYPE_EAGLE = 1;
	public static final int OBJECT_TYPE_PLAYER = 2;
	public static final int OBJECT_TYPE_ENEMY = 3;
	public static final int OBJECT_TYPE_BULLET = 4;
	public static final int OBJECT_TYPE_POWERUP = 5;

	//Direction constants
	public enum Direction {
		UP, DOWN,LEFT, RIGHT;
	};

	private int type;
	
	public GameObject(int type, Image img){
		super(img);
		this.type = type;
	}
	
	public abstract boolean onCollide(GameObject object);
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
