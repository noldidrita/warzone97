package model;

import java.util.ArrayList;

import view.SceneManager;

public class FreezeEnemiesPowerUp extends PowerUp {

	public FreezeEnemiesPowerUp() {
		super(OBJECT_TYPE_POWERUP, SceneManager.getResources().getImage(
				"freezetime_powerup.png"), 15, 30);
	}

	@Override
	public void applyEffect(Tank o) {
		ArrayList<GameObject> objects = SceneManager.getLevel()
				.getGameObjects();
		for (int i = 0; i < objects.size(); i++) {
			GameObject curr = objects.get(i);
			if (curr instanceof Tank) {
				if (((Tank) curr).getType() == GameObject.OBJECT_TYPE_ENEMY)
					((Tank) curr).pauseAnimations();
			}
		}
	}

	@Override
	public void removeEffect(Tank o) {
		if (SceneManager.getLevel() != null) {
			ArrayList<GameObject> objects = SceneManager.getLevel()
					.getGameObjects();
			for (int i = 0; i < objects.size(); i++) {
				GameObject curr = objects.get(i);
				if (curr instanceof Tank) {
					if (((Tank) curr).getType() == GameObject.OBJECT_TYPE_ENEMY)
						((Tank) curr).resumeAnimations();
				}
			}
		}
	}
}
