package model;

import view.SceneManager;

public class ShieldPowerUp extends PowerUp{
	public ShieldPowerUp() {
		super(OBJECT_TYPE_POWERUP, SceneManager.getResources().getImage("shield_powerup.png"), 15, 20);
	}

	@Override
	public void applyEffect(Tank o) {
		o.setHasShield(true);

	}

	@Override
	public void removeEffect(Tank o) {
		o.setHasShield(false);
	}
}
