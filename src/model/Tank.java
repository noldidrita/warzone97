package model;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.effect.Bloom;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import view.GameLevel;
import view.SceneManager;

public class Tank extends GameObject {

	private IntegerProperty healthPoints;
	private IntegerProperty lives;
	private Image defaultImg;
	private int tankNr;
	private Direction direction;
	private long lastShoot;
	private int shootInterval;
	private int bulletDamage;
	private int bulletSpeed;
	private Image bulletImage;
	private int moveSpeed;
	private Timeline animation;
	private boolean hasShield;
	private boolean dead;
	private boolean inLevel;
	private int maxHp;
	private FadeTransition fade;
	private SpriteAnimation motionAnimation;
	private Timeline AI;
	private int followObject;
	private Animation.Status motionStatus, animationStatus;

	public Tank(int type, Image defaultImg, int healthPoints, int lives,
			int tankNr, int shootInterval, int bulletDamage, int bulletSpeed,
			Image bulletImage, int moveSpeed, int followObject) {
		super(type, defaultImg);
		this.healthPoints = new SimpleIntegerProperty(healthPoints);
		maxHp = healthPoints;
		this.lives = new SimpleIntegerProperty(lives);
		this.defaultImg = defaultImg;
		this.tankNr = tankNr;
		this.shootInterval = shootInterval;
		this.bulletDamage = bulletDamage;
		this.bulletSpeed = bulletSpeed;
		this.bulletImage = bulletImage;
		this.moveSpeed = moveSpeed;
		this.followObject = followObject;
		direction = Direction.UP;
		setRotate(270);
		dead = true;
		inLevel = false;
		motionAnimation = new SpriteAnimation(this, Duration.millis(1000), 8,
				8, 29, 29);
		setViewport(new Rectangle2D(0, 0, 29, 29));
		motionAnimation.setCycleCount(Animation.INDEFINITE);
	}

	public void draw(Pane level, double x, double y) {
		setX(x);
		setY(y);
		setVisible(true);
		healthPoints.set(maxHp);
		if (tankNr == 7) {
			fade = new FadeTransition(Duration.millis(1000), this);
			fade.setFromValue(1);
			fade.setToValue(0.5);
			fade.setCycleCount(Animation.INDEFINITE);
			fade.setAutoReverse(true);
			fade.play();
		} else {
			fade = new FadeTransition(Duration.millis(500), this);
			fade.setFromValue(1);
			fade.setToValue(0);
			fade.setCycleCount(6);
			fade.setAutoReverse(true);
			fade.play();
		}
		if (animation == null) {
			animation = new Timeline(new KeyFrame(Duration.millis(20),
					new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							if (direction != null) {
								double x = getX();
								double y = getY();
								switch (direction) {
								case UP:
									y -= moveSpeed;
									break;
								case DOWN:
									y += moveSpeed;
									break;
								case LEFT:
									x -= moveSpeed;
									break;
								case RIGHT:
									x += moveSpeed;
									break;
								}
								GameLevel level = SceneManager.getLevel();
								if (level != null) {
									if (level.canMove(Tank.this, x, y)) {
										if (tankNr < 2)
											SceneManager
													.getLevel()
													.setTargetPosition(
															tankNr,
															x
																	+ Tank.this
																			.getFitWidth()
																	/ 2,
															y
																	+ Tank.this
																			.getFitHeight()
																	/ 2);
										setX(x);
										setY(y);
										SceneManager.getLevel().onMove(
												Tank.this);
									}
								}
							}
						}
					}));
			animation.setCycleCount(Animation.INDEFINITE);
		}
		dead = false;
		if (!inLevel) {
			level.getChildren().add(this);
			inLevel = true;
		}
		if (tankNr > 1) {
			AI = new Timeline(new KeyFrame(Duration.millis(200),
					new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							if (SceneManager.getLevel() == null) {
								AI.stop();
								return;
							}
							double[] target = SceneManager.getLevel()
									.getTargetPosition(followObject);
							if (target[0] == -1) {
								followObject = 1 - followObject;
								target = SceneManager.getLevel()
										.getTargetPosition(followObject);
							}
							double distx = target[0] - getX();
							double disty = target[1] - getY();
							if (Math.abs(distx) > Math.abs(disty)) {
								if (distx < 0)
									onMove(Direction.LEFT);
								else
									onMove(Direction.RIGHT);
							} else {
								if (disty < 0)
									onMove(Direction.UP);
								else
									onMove(Direction.DOWN);
							}
							onShoot();
						}
					}));
			AI.setCycleCount(Animation.INDEFINITE);
			AI.play();
		}
	}

	public void pauseAnimations() {

		if (animation != null) {
			animationStatus = animation.getStatus();
			animation.stop();
		}
		if (AI != null)
			AI.stop();
		if (motionAnimation != null) {
			motionStatus = motionAnimation.getStatus();
			motionAnimation.stop();
		}
	}

	public void resumeAnimations() {
		if (animation != null
				&& animationStatus.equals(Animation.Status.RUNNING))
			animation.play();
		if (AI != null)
			AI.play();
		if (motionAnimation != null
				&& motionStatus.equals(Animation.Status.RUNNING))
			motionAnimation.play();
	}

	public void onShoot() {
		long interval = System.currentTimeMillis() - lastShoot;
		if (interval >= shootInterval) {
			// Center values for this image;
			double x = getX() + getBoundsInParent().getWidth() / 2;
			double y = getY() + getBoundsInParent().getHeight() / 2;
			switch (direction) {
			case UP:
				y -= getBoundsInLocal().getHeight() / 2
						+ bulletImage.getHeight();
				x -= bulletImage.getWidth() / 2;
				break;
			case DOWN:
				y += getBoundsInLocal().getHeight() / 2;
				x -= bulletImage.getWidth() / 2;
				break;
			case LEFT:
				x -= getBoundsInLocal().getWidth() / 2 + bulletImage.getWidth();
				y -= bulletImage.getHeight() / 2;
				break;
			case RIGHT:
				x += getBoundsInLocal().getWidth() / 2;
				y -= bulletImage.getHeight() / 2;
				break;
			}
			SceneManager.getSoundManager().TankFire();
			Bullet b = new Bullet(bulletDamage, getType(), x, y, direction,
					bulletSpeed, bulletImage);
			lastShoot = System.currentTimeMillis();
		}
	}

	public void onMove(Direction dir) {
		direction = dir;
		double rotation = 0;
		switch (direction) {
		case UP:
			rotation = 270;
			break;
		case DOWN:
			rotation = 90;
			break;
		case LEFT:
			rotation = 180;
			break;
		case RIGHT:
			rotation = 0;
			break;
		}
		setRotate(rotation);

		motionAnimation.play();
		animation.play();
	}

	public void onStop() {
		motionAnimation.stop();
		animation.stop();
		setViewport(new Rectangle2D(0, 0, 29, 29));
	}

	@Override
	public boolean onCollide(GameObject object) {
		if (object.getType() == OBJECT_TYPE_BULLET) {
			if (hasShield)
				return true;
			Bullet b = (Bullet) object;
			if (b.getSource() != getType()) {
				healthPoints.set(healthPoints.get() - b.getDamage());
				if (healthPoints.get() <= 0) {
					if (getType() == OBJECT_TYPE_ENEMY) {
						if (tankNr == 7) {
							if (fade != null)
								fade.stop();
							showRandomPowerUp();
						}
						if (tankNr > 1)
							if (AI != null)
								AI.stop();			
						SceneManager.getSoundManager().TankExplosion();
						SceneManager.getLevel().removeObjectFromList(this);
						SceneManager.getLevel().removeNodeFromPane(this);
						SceneManager.updateScore(10 * bulletDamage);
					} else {
						SceneManager.getLevel().setTargetPosition(tankNr, -1,
								-1);
						dead = true;
						SceneManager.getSoundManager().TankExplosion();
						lives.set(lives.get() - 1);
						setVisible(false);
					}
				}
			}
		}
		return true;
	}

	public void destroy() {
		if (tankNr == 7) {
			if (fade != null)
				fade.stop();
			showRandomPowerUp();
		}
		if (tankNr > 1)
			if (AI != null)
				AI.stop();
		SceneManager.getLevel().removeObjectFromList(this);
		SceneManager.getLevel().removeNodeFromPane(this);
		SceneManager.updateScore(10 * bulletDamage);
	}

	private void showRandomPowerUp() {
		int nr = SceneManager.getLevel().getRandom().nextInt(6);
		PowerUp pu = null;
		switch (nr) {
		case 0:
			pu = new HealthPowerUp();
			break;
		case 1:
			pu = new ShieldPowerUp();
			break;
		case 2:
			pu = new SpeedPowerUp();
			break;		
		case 3:
			pu = new FreezeEnemiesPowerUp();
			break;
		case 4:
			pu = new DestroyEnemiesPowerUp();
			break;
		case 5:
			pu = new BetterBulletPowerUp();
			break;
		}
		if (pu != null) {
			pu.setX(getX());
			pu.setY(getY());
			SceneManager.getLevel().addObjectToList(pu);
			pu.draw(SceneManager.getLevel().getLevelPane(), getX(), getY());
		}
	}

	public void setHasShield(boolean hasShield) {
		if (hasShield)
			this.setEffect(new Bloom(0.01));
		else
			this.setEffect(new Bloom(1));
		this.hasShield = hasShield;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public long getLastShoot() {
		return lastShoot;
	}

	public void setLastShoot(long lastShoot) {
		this.lastShoot = lastShoot;
	}

	public int getShootInterval() {
		return shootInterval;
	}

	public void setShootInterval(int shootInterval) {
		this.shootInterval = shootInterval;
	}

	public int getBulletDamage() {
		return bulletDamage;
	}

	public void setBulletDamage(int bulletDamage) {
		this.bulletDamage = bulletDamage;
	}

	public int getBulletSpeed() {
		return bulletSpeed;
	}

	public void setBulletSpeed(int bulletSpeed) {
		this.bulletSpeed = bulletSpeed;
	}

	public Image getBulletImage() {
		return bulletImage;
	}

	public void setBulletImage(Image bulletImage) {
		this.bulletImage = bulletImage;
	}

	public int getMoveSpeed() {
		return moveSpeed;
	}

	public void setMoveSpeed(int moveSpeed) {
		this.moveSpeed = moveSpeed;
	}

	public IntegerProperty getHealthPoints() {
		return healthPoints;
	}

	public void setHealthPoints(IntegerProperty healthPoints) {
		this.healthPoints = healthPoints;
	}

	public IntegerProperty getLivesProperty() {
		return lives;
	}

	public int getLives() {
		return lives.get();
	}

	public void setLives(IntegerProperty lives) {
		this.lives = lives;
	}

	public Image getDefaultImg() {
		return defaultImg;
	}

	public void setDefaultImg(Image defaultImg) {
		this.defaultImg = defaultImg;
	}

	public int getTankNr() {
		return tankNr;
	}

	public void setTankNr(int nr) {
		this.tankNr = nr;
	}

	public boolean isDead() {
		return dead;
	}

	public void setHealth(int hp) {
		healthPoints.set(hp);
	}
}
