package model;

import javafx.scene.image.Image;
import view.SceneManager;

public class Treasure extends GameObject{

	public Treasure(Image img) {
		super(OBJECT_TYPE_EAGLE, img);
	}

	@Override
	public boolean onCollide(GameObject o) {
		SceneManager.onGameOver(false, SceneManager.getLevel().getNrPlayers());
		return true;
	}
}
