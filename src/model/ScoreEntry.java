package model;

import java.io.Serializable;

public class ScoreEntry implements Comparable, Serializable{
	public String name;
	public String date;
	public int score;

	public ScoreEntry(String name, String date, int score) {
		super();
		this.name = name;
		this.score = score;
		this.date = date;
	}
	
	@Override
	public String toString() {
		return String.format("%s    Score: %d    Date: %s", name, score, date);
	}
		
	@Override
	public int compareTo(Object o) {
		return ((ScoreEntry)o).score-score;
	}
}
