package model;

import javafx.scene.image.Image;

public class EnemyTank extends Tank {

	public EnemyTank(int type, Image defaultImg, int healthPoints, int lives,
			int tankNr, int shootInterval, int bulletDamage, int bulletSpeed,
			Image bulletImage, int moveSpeed) {
		super(type, defaultImg, healthPoints, lives, tankNr, shootInterval,
				bulletDamage, bulletSpeed, bulletImage, moveSpeed, -1);
	}
}
