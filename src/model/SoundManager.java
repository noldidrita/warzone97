package model;

import java.io.File;

import view.SceneManager;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class SoundManager {
	private MediaPlayer GamePlaySound;
	private AudioClip BrickHitPlayer, BulletOnMetalPlayer, PowerUpPlayer, TankExplosionPlayer, TankFirePlayer;
	
	public SoundManager(){
		BrickHitPlayer = SceneManager.getResources().getSound("BrickHit.mp3");
		BulletOnMetalPlayer = SceneManager.getResources().getSound("BulletOnMetal.mp3");
		PowerUpPlayer = SceneManager.getResources().getSound("PowerUp.mp3");
		TankExplosionPlayer = SceneManager.getResources().getSound("TankExplosion.mp3");
		TankFirePlayer =SceneManager.getResources().getSound("TankFire.mp3");
		
	}
	public void BrickHit(){
		BrickHitPlayer.setCycleCount(1);
		BrickHitPlayer.play();
	}
	public void BulletOnMetal(){
		BulletOnMetalPlayer.setCycleCount(1);
		BulletOnMetalPlayer.play();
	}	
	public void PowerUpPlayer(){
		PowerUpPlayer.setCycleCount(1);
		PowerUpPlayer.play();
	}	
	public void TankExplosion(){
		TankExplosionPlayer.setCycleCount(1);
		TankExplosionPlayer.play();
	}	
	public void TankFire(){
		TankFirePlayer.setCycleCount(1);
		TankFirePlayer.play();
	}
	public void GamePlaySound(){
		GamePlaySound = new MediaPlayer(new Media(new File("sounds/gamePlay.mp3").toURI().toString()));
		GamePlaySound.setVolume(0.3);
		GamePlaySound.setOnReady(new Runnable() {
			@Override
			public void run() {
				GamePlaySound.play();
			}
		});
	}
}
