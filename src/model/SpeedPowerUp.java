package model;

import view.SceneManager;

public class SpeedPowerUp extends PowerUp{

	public SpeedPowerUp() {
		super(OBJECT_TYPE_POWERUP, SceneManager.getResources().getImage("speed_powerup.png"), 25, 15);
	}

	@Override
	public void applyEffect(Tank o) {
		o.setMoveSpeed(o.getMoveSpeed()+2);
	}

	@Override
	public void removeEffect(Tank o) {
		o.setMoveSpeed(o.getMoveSpeed()-2);
	}
}
