package model;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import view.SceneManager;

public class Bullet extends GameObject{
	
	private int damage;
	private int source;
	private Direction direction;
	private Timeline animation;
	
	public int getDamage() {
		return damage;
	}

	public int getSource() {
		return source;
	}
	
	public void setDamage(int damage) {
		this.damage = damage;
	}

	public Bullet(int damage, int source, double x, double y, Direction dir, int speed, Image img) {
		super(OBJECT_TYPE_BULLET, img);
		this.direction = dir;
		this.damage = damage;
		this.source = source;
		setX(x);
		setY(y);
		SceneManager.getLevel().draw(this);
		SceneManager.getLevel().addObjectToList(this);;

		animation = new Timeline(new KeyFrame(Duration.millis(20), new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				double x = getX();
				double y = getY();
				switch(dir){
				case UP:	y-=speed;
					break;
				case DOWN: y+=speed;
					break;
				case LEFT: x-=speed;
					break;				
				case RIGHT: x+=speed;
					break;
				}
				if(SceneManager.getLevel()==null){
					animation.stop();
					return;
				}

				setX(x);
				setY(y);
				if(x<=0 || x>=SceneManager.getLevel().getWidth() || y<=0 || y>=SceneManager.getLevel().getHeight())
				{
					animation.stop();
					SceneManager.getLevel().removeNodeFromPane(Bullet.this);
					SceneManager.getLevel().removeObjectFromList(Bullet.this);
				}
				else{
					SceneManager.getLevel().onMove(Bullet.this);
				}
			}
		}));
		animation.setCycleCount(Animation.INDEFINITE);
		animation.play();
	}
	
	public void pauseAnimations(){
		if(animation!=null)
			animation.pause();
	}
	
	public void resumeAnimations(){
		if(animation!=null)
			animation.play();
	}
	@Override
	public boolean onCollide(GameObject object) {
		animation.stop();
		if(SceneManager.getLevel()!=null){
			SceneManager.getLevel().removeNodeFromPane(this);
			SceneManager.getLevel().removeObjectFromList(this);
			ImageView imgView = new ImageView(SceneManager.getResources().getImage("explosion.png"));
			SpriteAnimation explosion = new SpriteAnimation(imgView, Duration.seconds(1), 45, 9, 64, 48);
			double x = getX() + getBoundsInParent().getWidth() / 2;
			double y = getY() + getBoundsInParent().getHeight() / 2;
			imgView.setX(x - 32);
			imgView.setY(y - 24);
			imgView.setViewport(new Rectangle2D(0, 0, 64, 48));
			explosion.setOnFinished(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {
					if(SceneManager.getLevel()!=null)
						SceneManager.getLevel().removeNodeFromPane(imgView);
				}
			});
			explosion.setCycleCount(1);
		    explosion.play();
		    SceneManager.getLevel().draw(imgView);
		}
		return true;
	}

	public Direction getDirection() {
		return direction;
	}

}
