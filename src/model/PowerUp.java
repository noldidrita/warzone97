package model;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import view.SceneManager;

public abstract class PowerUp extends GameObject{
	
	private int duration;
	private Timeline action;
	private int score;
	
	public PowerUp(int type, Image img, int duration, int score) {
		super(OBJECT_TYPE_POWERUP, img);
		this.duration = duration;
		this.score = score;
	}
	
	public abstract void applyEffect(Tank o);
	public abstract void removeEffect(Tank o);
	
	@Override
	public boolean onCollide(GameObject object) {
		if(object.getType()==OBJECT_TYPE_PLAYER){
			applyEffect((Tank)object);
			SceneManager.getSoundManager().PowerUpPlayer();
			if(duration!=-1){
				action = new Timeline(new KeyFrame(Duration.seconds(duration), new EventHandler<ActionEvent>(){
					@Override
					public void handle(ActionEvent event) {
						removeEffect((Tank)object);
					}
				}));
				action.setCycleCount(1);
				action.play();
			}
			SceneManager.updateScore(score);
			SceneManager.getLevel().removeObjectFromList(this);
			SceneManager.getLevel().removeNodeFromPane(this);
			return true;
		}
		return false;
	}
	
	public void draw(Pane level, double x, double y){
		setX(x);
		setY(y);
		level.getChildren().add(this);
	}
}
