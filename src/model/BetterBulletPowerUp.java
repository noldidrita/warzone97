package model;

import view.SceneManager;

public class BetterBulletPowerUp extends PowerUp{

	public BetterBulletPowerUp() {
		super(OBJECT_TYPE_POWERUP, SceneManager.getResources().getImage("bullet_powerup.png"), 90, 15);
	}

	@Override
	public void applyEffect(Tank o) {
		o.setBulletSpeed(o.getBulletSpeed()+3);
		o.setBulletDamage(o.getBulletDamage()+20);

	}

	@Override
	public void removeEffect(Tank o) {
		o.setBulletSpeed(o.getBulletSpeed()-3);
		o.setBulletDamage(o.getBulletDamage()-20);
	}
}
