package model;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
/*
 * This class was taken from the following website: http://blog.netopyr.com/2012/03/09/creating-a-sprite-animation-with-javafx/
 * The technique is a very old one, but this way of implementing it with javafx was
 * convenient, so I just took the code from this site.
 */

public class SpriteAnimation extends Transition {

    private final ImageView imageView;
    private final int count;
    private final int width;
    private final int height;
    private final int columns;
    private int lastIndex;

    public SpriteAnimation(
            ImageView imageView, 
            Duration duration, 
            int count, int columns,
            int width,   int height) {
        this.imageView = imageView;
        this.count     = count;
        this.width     = width;
        this.height    = height;
        this.columns = columns;
        setCycleDuration(duration);
        setInterpolator(Interpolator.LINEAR);
    }

    protected void interpolate(double k) {
        final int index = Math.min((int) Math.floor(k * count), count - 1);
        if (index != lastIndex) {
            final int x = (index % columns) * width;
            final int y = (index / columns) * height;
            imageView.setViewport(new Rectangle2D(x, y, width, height));
            lastIndex = index;
        }
    }
}