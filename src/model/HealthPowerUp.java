package model;

import view.SceneManager;

public class HealthPowerUp extends PowerUp {
	
	public HealthPowerUp() {
		super(OBJECT_TYPE_POWERUP, SceneManager.getResources().getImage("health_powerup.png"), -1, 15);
	}

	@Override
	public void applyEffect(Tank o) {
			o.setHealth(100);
	}

	@Override
	public void removeEffect(Tank o) {
	}
}
