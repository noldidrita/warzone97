package model;

import java.util.ArrayList;

import view.SceneManager;

public class DestroyEnemiesPowerUp extends PowerUp{

	public DestroyEnemiesPowerUp() {
		super(OBJECT_TYPE_POWERUP, SceneManager.getResources().getImage("killenemies_powerup.png"), -1, 15);
	}

	@Override
	public void applyEffect(Tank o) {
		ArrayList<GameObject> objects = SceneManager.getLevel().getGameObjects();
		for(int i=0; i<objects.size(); i++){
			if(objects.get(i) instanceof Tank){
				if(((Tank)(objects.get(i))).getType()==GameObject.OBJECT_TYPE_ENEMY)
					((Tank)(objects.get(i))).destroy();
			}
		}
	}

	@Override
	public void removeEffect(Tank o) {
	}
}
