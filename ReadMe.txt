This is a game I have developed as my project assignment for Object Oriented Programming with Java course. 
It is a game implemented on top of JavaFX framework with Java 8 and it uses a somewhat strict MVP organization. 
It is a clone of the popular Battle City NES game with some improvements on the gameplay/AI/graphics. 
The project was completed in a weekend so I'd assume in around 23-35 hours. 
However it lacks proper documentation. 

Whenever I can find some time I will document it properly.

Instructions on adding levels
------------------------------------------------------------------------------------------------
To add new levels, create a new text file, with the extension .txt
Add this file to the levels folder and modify the levels.cfg file by adding your level name
In the level txt file, use the numbers 1-8 for bricks, 0 for blank space and the number 9 for the treasure chest.
Every level must have a treasure chest.
The file is organized like this:
First two numbers, defining the dimensions of the grid
Then the grid is given with the numbers as stated above.
Then two numbers, stating the total enemy count for this level and the number of enemies that can be 
visible in the field at any given time.
Then lastly a background picture for this level.
The last three parameters are optional, but the grid size and the grid itself are required.